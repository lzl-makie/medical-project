import request from '@/utils/request'
// 请求科室数据
export const getKeshi = () => {
  return request({
    // url: '/xiongmao/api/tab/1',
    url: 'http://rap2api.taobao.org/app/mock/data/1845230',
    method: 'GET'
  })
}

// 请求医院数据
export const getHospital = () => {
  return request({
    // url: '/xiongmao/api/tab/1',
    url: 'http://rap2api.taobao.org/app/mock/274778/hospital/list',
    method: 'GET'
  })
}

// 请求预约医生数据
export const doc = () => {
  return request({
    // url: '/xiongmao/api/tab/1',
    url: 'http://rap2api.taobao.org/app/mock/274778/doctor/data',
    method: 'GET'
  })
}
