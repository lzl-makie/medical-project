import request from '@/utils/request'

export const getHome = () => {
  return request({
    url: 'http://rap2api.taobao.org/app/mock/274778/doctor/data',
    method: 'GET'
  })
}
