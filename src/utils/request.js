/* 实现 axios 二次封装 */
import axios from 'axios'
import { Toast } from 'vant'

// development：开发环境  production：生产环境
// const baseURL = process.env.NODE_ENV === 'development' ? 'rap2' : 'real'
// const baseURL = process.env.NODE_ENV === 'development' ? 'rap2' : 'http://www.xiongmaoyouxuan.com'

// 创建 axios 实例
const service = axios.create({
  //   baseURL: 'http://www.xiongmaoyouxuan.com',
  //   baseURL
  baseURL: '/xiongmao/api',
  timeout: 3000// 超时
})

// 请求拦截
service.interceptors.request.use(config => {
  // 统一添加请求加载提示效果
  Toast.loading({
    message: '加载中...',
    forbidClick: true,
    duration: 0
  })

  // 向请求头中添加相关的 Header头信息
  config.header = {
    'X-Token': 'osadasdas'
  }

  return config
})

// 响应拦截
service.interceptors.response.use(res => {
  // 关闭轻提示
  Toast.clear()
  // 对响应数据做处理
  if (res.status >= 200 && res.status < 300) {
    // 获取从后端中拿到的数据，该数据是前后端接口交互过程中的规范数据
    const apiData = res.data
    // 判断后端数据规范
    if (apiData.code === 200) { // 能够拿到成功的数据
      return apiData.data
    }
    // 如果 apiData.code 不为 200 则说明数据获取异常
    const err = new Error('API 接口数据异常')
    err.response = apiData
    return Promise.reject(err)
  }

  // res.status 不以 2xx 开头，则说明请求可能异常
  const e = new Error('API 请求异常')
  e.response = res
  return Promise.reject(e)
  //   console.log('=========================')
  //   console.log('请求结果：', res)
  //   console.log('=========================')
  //   return res
})

export default service
