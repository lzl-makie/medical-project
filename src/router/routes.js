// @ 是 src 目录的别名
import Home from '@/views/home'
import Mine from '@/views/mine'
import Science from '@/views/science'
import Message from '@/views/message'
import Login from '@/views/login'
import Register from '@/views/register'

// 我的页面开始--------
import Setting from '@/views/mine/setting'
import Follow from '@/views/mine/follow'
import Coupon from '@/views/mine/coupon'
import Consultation from '@/views/mine/consultation'
import Prescription from '@/views/mine/prescription'
import Recipel from '@/views/mine/recipel'
import Family from '@/views/mine/family'
import Address from '@/views/mine/address'
import Push from '@/views/mine/setting/push'
import Password from '@/views/mine/setting/password'
// 我的页面结束--------
import List from '@/views/science/list'
import Detail from '@/views/science/list/detail'

import Opinion from '@/views/mine/opinion'
import Order from '@/views/mine/order'

import OpenMedical from '@/views/home/open-medical'
import Application from '@/views/home/application'
import Ask from '@/components/ask'
import Askdetail from '@/components/askdetail'
import Pay from '@/components/pay'
import AddPatiens from '@/components/add-patiens'
import FindMedical from '@/components/find-medical'
import UseMedical from '@/components/use-medical'
import PaySuccess from '@/components/pay-success'
// 问诊开始-----------
import AskBody from '@/views/home/askBody' // 问诊首页
import AskDeser from '@/views/home/askBody/askDeser' // 按照疾病
import AskHospital from '@/views/home/askBody/askHospital' // 按照医院
// 问诊结束------------

// 挂号开始==========
import GetNumber from '@/views/home/getnumber' // 挂号首页
import Disease from '@/views/home/getnumber/disease' // 挂号-按照疾病
import Hospital from '@/views/home/getnumber/hospital' // 挂号-按照医院
import Hosdetails from '@/views/home/getnumber/hospital/hosdetails' // 医院详情页
import HosDesc from '@/views/home/getnumber/hospital/hosdetails/hosDesc' // 医院详情页--医院简介
import HosClasses from '@/views/home/getnumber/hospital/hosdetails/hosClass' // 医院详情页--科室
import DocIndex from '@/views/home/getnumber/docIndex' // 医生主页
import DocJianjie from '@/views/home/getnumber/docIndex/docJianjie' // 医生简介
import AskLine from '@/views/home/getnumber/docIndex/askLine' // 在线问诊
import OrderNumber from '@/views/home/getnumber/docIndex/orderNumber' // 预约挂号
import OrderRulers from '@/views/home/getnumber/docIndex/orderNumber/orderRulers' // 预约挂号
import GuaHao from '@/views/home/getnumber/docIndex/orderNumber/guahao' // 挂号
import Success from '@/views/home/getnumber/docIndex/orderNumber/guahao/success' // 预约成功
import Finish from '@/views/home/getnumber/docIndex/orderNumber/guahao/success/finish' // 预约完成
// 挂号结束============
import TabBar from '@/components/tab-bar'
import NavBar from '@/components/nav-bar'

// 路由路径的配置数组
const routes = [
  {
    path: '/',
    redirect: '/home' // 重定向
  },
  {
    path: '/home',
    name: 'Home', // 命名路由
    // component: Home
    components: {
      header: NavBar,
      footer: TabBar,
      default: Home
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      isTab: true, // true 表示是需要添加到 TabBar 上显示渲染并跳转的路由
      title: '首页',
      icon: 'home-o',
      isSearch: true, // 是否在导航栏显示搜索框
      isHome: true // 是否是首页
    }
  },
  {
    path: '/science',
    name: 'Science', // 命名路由
    // component: science
    components: {
      header: NavBar,
      footer: TabBar,
      default: Science
    },
    meta: {
      isTab: true, // true 表示是需要添加到 TabBar 上显示渲染并跳转的路由
      title: '疾病科普',
      icon: 'service-o',
      isHome: false
    }
  },
  {
    path: '/message',
    name: 'Message', // 命名路由
    // component: message
    components: {
      header: NavBar,
      footer: TabBar,
      default: Message
    },
    meta: { // 路由元信息
      isTab: true, // true 表示是需要添加到 TabBar 上显示渲染并跳转的路由
      title: '消息',
      icon: 'comment-o',
      badge: 5,
      isHome: false
    }
  },
  {
    path: '/mine',
    name: 'Mine',
    // component: Mine
    components: {
      footer: TabBar,
      default: Mine
    },
    meta: { // 路由元信息
      isTab: true, // true 表示是需要添加到 TabBar 上显示渲染并跳转的路由
      title: '我的',
      icon: 'user-o'
    }
  },
  {
    path: '/home/open-medical',
    name: 'OpenMedical',
    // component: Mine
    components: {
      default: OpenMedical
    }
  },
  {
    path: '/home/application',
    name: 'Application',
    components: {
      default: Application
    }
  },
  {
    path: '/home/ask',
    name: 'Ask',
    components: {
      default: Ask
    }
  },
  {
    path: '/home/askdetail',
    name: 'Askdetail',
    components: {
      default: Askdetail
    }
  },
  {
    path: '/home/pay',
    name: 'Pay',
    components: {
      default: Pay
    }
  },
  {
    path: '/home/addPatiens',
    name: 'AddPatiens',
    components: {
      default: AddPatiens
    }
  },
  // 挂号开始
  {
    path: '/home/getnumber',
    name: 'GetNumber',
    components: {
      default: GetNumber
    }
  },
  {
    path: '/home/disease',
    name: 'Disease',
    components: {
      default: Disease
    }
  },
  {
    path: '/home/hospital',
    name: 'Hospital',
    components: {
      default: Hospital
    }
  },
  {
    path: '/home/hosdetails',
    name: 'Hosdetails',
    components: {
      default: Hosdetails
    }
  },
  {
    path: '/home/hosDesc',
    name: 'HosDesc',
    components: {
      default: HosDesc
    }
  },
  {
    path: '/home/hosClass',
    name: 'HosClasses',
    components: {
      default: HosClasses
    }
  },
  {
    path: '/home/docIndex',
    name: 'DocIndex',
    components: {
      default: DocIndex
    }
  },
  {
    path: '/home/docJianjie',
    name: 'DocJianjie',
    components: {
      default: DocJianjie
    }
  },
  {
    path: '/home/askLine', // 在线询问
    name: 'AskLine',
    components: {
      default: AskLine
    }
  },
  {
    path: '/home/orderNumber', // 预约挂号
    name: 'OrderNumber',
    components: {
      default: OrderNumber
    }
  },
  {
    path: '/home/orderRulers', // 预约规则
    name: 'OrderRulers',
    components: {
      default: OrderRulers
    }
  },
  {
    path: '/home/guahao', // 挂号
    name: 'GuaHao',
    components: {
      default: GuaHao
    }
  },
  {
    path: '/home/success', // 挂号完成
    name: 'Success',
    components: {
      default: Success
    }
  },
  {
    path: '/home/finish', // 预约完成
    name: 'Finish',
    components: {
      default: Finish
    }
  },
  // 挂号结束
  {
    path: '/home/find-medical',
    name: 'FindMedical',
    components: {
      default: FindMedical
    }
  },
  {
    path: '/home/use-medical',
    name: 'UseMedical',
    components: {
      default: UseMedical
    }
  },
  {
    path: '/mine/setting',
    name: 'Setting',
    // component: Mine
    components: {
      header: NavBar,
      default: Setting
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '设置',
      isHome: false // 是否是首页
    }
  },
  {
    path: '/mine/coupon',
    name: 'Coupon',
    // component: Mine
    components: {
      header: NavBar,
      default: Coupon
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '优惠券',
      isHome: false // 是否是首页
    }
  },
  {
    path: '/mine/follow',
    name: 'Follow',
    // component: Mine
    components: {
      header: NavBar,
      default: Follow
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '关注的医生',
      isHome: false // 是否是首页
    }
  },
  {
    path: '/mine/consultation',
    name: 'Consultation',
    // component: Mine
    components: {
      header: NavBar,
      default: Consultation
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '问诊',
      isHome: false // 是否是首页
    }
  },
  {
    path: '/mine/prescription',
    name: 'Prescription',
    // component: Mine
    components: {
      header: NavBar,
      default: Prescription
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '开药',
      isHome: false
    }
  }, // 是否是首页
  // 问诊开始
  {
    path: '/home/askBody',
    name: 'AskBody',
    components: {
      default: AskBody
    }
  },
  {
    path: '/home/askHospital',
    name: 'AskHospital',
    components: {
      default: AskHospital
    }
  },
  {
    path: '/home/askDeser',
    name: 'AskDeser',
    components: {
      default: AskDeser
    }
  },
  {
    path: '/science/list',
    name: 'List',
    components: {
      header: NavBar,
      default: List
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '糖尿病',
      isHome: false
    }
  },
  {
    path: '/setting/push',
    name: 'Push',
    components: {
      header: NavBar,
      default: Push
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '推送设置',
      isHome: false
    }
  },
  {
    path: '/setting/password',
    name: 'Password',
    components: {
      header: NavBar,
      default: Password
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '修改密码',
      isHome: false
    }
  },
  {
    path: '/science/list/detail',
    name: 'Detail',
    components: {
      header: NavBar,
      default: Detail
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '详细页',
      isHome: false
    }
  },
  // 问诊结束
  {
    path: '/mine/opinion',
    name: 'Opinion',
    components: {
      header: NavBar,
      default: Opinion
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '意见反馈',
      isHome: false // 是否是首页
    }
  },
  {
    path: '/mine/order',
    name: 'Order',
    components: {
      header: NavBar,
      default: Order
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '药品订单',
      isHome: false // 是否是首页
    }
  },
  {
    path: '/mine/recipel',
    name: 'Recipel',
    // component: Mine
    components: {
      header: NavBar,
      default: Recipel
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '我的处方',
      isHome: false
    }
  },
  // 处方结束
  {
    path: '/mine/family',
    name: 'Family',
    // component: Mine
    components: {
      header: NavBar,
      default: Family
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '家庭档案',
      isHome: false
    }
  },
  // 家庭档案结束
  {
    path: '/mine/address',
    name: 'Address',
    // component: Mine
    components: {
      header: NavBar,
      default: Address
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '地址管理',
      isHome: false
    }
  },
  {
    path: '/home/pay-success',
    name: 'PaySuccess',
    // component: Mine
    components: {
      header: NavBar,
      default: PaySuccess
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '支付成功',
      isHome: false
    }
  },
  {
    path: '/home/register',
    name: 'Register',
    components: {
      header: NavBar,
      default: Register
    },
    meta: { // 路由元信息，保存当前路由相关的一些数据，业务开发中可能会使用到
      title: '注册',
      isHome: false
    }
  },
  {
    path: '/home/login',
    name: 'Login',
    // component: Mine
    components: {
      default: Login
    }
  }
  // 地址管理结束
]

export default routes
