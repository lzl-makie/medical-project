// @ 是src 目录的别名
import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'

// 安装 VueRouter 路由的使用
Vue.use(VueRouter)

// 路由路径的配置

const router = new VueRouter({
  mode: 'history', // 路由模式
  routes
})

export default router
