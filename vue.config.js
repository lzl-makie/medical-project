// vue.config.js 是 Vue CLI 的配置文件，会在启动项目时（开发环境）或项目构建时自动加载该配置文件
module.exports = {
  css: {
    loaderOptions: {
      less: {
        // 若 less-loader 版本小于 6.0，请移除 lessOptions 这一级，直接配置选项。
        modifyVars: {
          // 直接覆盖变量
          'text-color': 'black',
          'text-color': '#000000',
          'tab-active-text-color': '#43240c',
          'tabs-bottom-bar-color': '#43240c',
          'tree-select-item-height': '55px',
          'tree-select-item-width': '140px',
          'tree-select-nav-item-padding': '0',
          'sidebar-width': '190px',
          'sidebar-selected-background-color': '#ccc',
          'sidebar-selected-border-height': '55px',
          'sidebar-selected-border-color': 'blue',
          'tree-select-item-active-color': '#323233'
        }
      }
    }
  }
}
